#include <iostream>
#include <cmath>

class Vector
{
public:

	Vector()
	{
		m_x = 0;
		m_y = 0;
		m_z = 0;
	}

	Vector(float x, float y, float z)
	{
		m_x = x;
		m_y = y;
		m_z = z;
	}

	friend Vector operator+(const Vector& a, const Vector& b);
	friend Vector operator-(const Vector& a, const Vector& b);
	friend Vector operator*(const Vector& a, int f);
	friend std::ostream& operator<<(std::ostream& out, const Vector& a);
	friend std::istream& operator>>(std::istream& in, Vector& a);
	friend bool operator>(const Vector& a, const Vector& b);

	float operator[](int i)
	{
		switch (i) {
		case 0:
			return m_x;
		case 1:
			return m_y;
		case 2:
			return m_z;
		default:
			return -1;
		}
	}

	operator float()
	{
		return std::sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
	}

private:
	float m_x, m_y, m_z;
};

Vector operator*(const Vector& a, int f)
{
	return Vector(a.m_x * f, a.m_y * f, a.m_z * f);
}

Vector operator+(const Vector& a, const Vector& b)
{
	return Vector(a.m_x + b.m_x, a.m_y + b.m_y, a.m_z + b.m_z);
}

Vector operator-(const Vector& a, const Vector& b)
{
	return Vector(a.m_x - b.m_x, a.m_y - b.m_y, a.m_z - b.m_z);
}

std::ostream& operator<<(std::ostream& out, const Vector& a)
{
	out << a.m_x << ' ' << a.m_y << ' ' << a.m_z;
	return out;
}

std::istream& operator>>(std::istream& in, Vector& a)
{
	in >> a.m_x >> a.m_y >> a.m_z;
	return in;
}

bool operator>(const Vector& a, const Vector& b)
{
	return false;
}

int main()
{
	Vector v1(0, 1, 2);
	Vector v2(3, 4, 5);
	Vector v3;
	v3 = v1 + v2;

	std::cout << "v1 + v2 = " << v3 << '\n';
	std::cout << v2[0] << ' ' << v2[1] << ' ' << v2[2] << ' ' << v2[43] << '\n';
	std::cout << "Length v1 = " << float(v1) << '\n';
	std::cout << "Length v2 = " << static_cast<float>(v2) << '\n';
	std::cout << "Length v3 = " << static_cast<float>(v3) << '\n';
	std::cout << "v3 * 4 = " << v3 * 4 << '\n';
	std::cout << "(4, 4, 4) - (4, 2, 2) = " << Vector(4, 4, 4) - Vector(4, 2, 2) << '\n';

	Vector v4;
	std::cout << "Enter vector elements in form 'x y z', or you'll get an error\n";
	std::cin >> v4;
	std::cout << "Vector v4: " << v4;
}

